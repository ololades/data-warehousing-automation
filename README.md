## Data Warehouse Automation

### Overview

This project aims to build an ETL pipeline that extracts data from cloud storage, stages data in BigQuery, and transforms data into a set of dimensional tables (using dbt).


### Implementation

The ETL pipeline is implemented using the following steps:

- **Data extraction**: extracting data from google cloud storage
- **Data staging**: creating a BigQuery table and loading the data into it
- **Data transformation**: utilize dbt to convert the data into a series of dimensional tables
- **Data loading**: load the transformed data into the data warehouse using the BigQuery client library


### Learnings
During the course of this project, I learned:

- ETL pipeline components, like data extraction, staging, transformation, and loading. I also discovered tools for each part.

- BigQuery fundamentals for data storage and querying, including advanced features like partitioned tables and materialized views.

- dbt basics for data transformation, including advanced elements such as macros and tests.
