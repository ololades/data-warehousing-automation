from google.cloud import storage
import pandas as pd
import subprocess
import os

# Google Cloud Storage details
bucket_name = 'your-bucket-name'
file_name = 'your-data-file.csv'

# BigQuery details
project_id = 'your-project-id'
dataset_id = 'your-dataset-id'
table_id = 'your-table-id'

# Step 1: Extract data from Google Cloud Storage
storage_client = storage.Client()
bucket = storage_client.get_bucket(bucket_name)
blob = bucket.blob(file_name)
blob.download_to_filename('/tmp/data.csv')


# step 2: stage in the data in bigquery




# Step 3: Transform data using dbt (You may need to customize this part)
# Example: Run a dbt transformation command
# Replace with your own dbt command
dbt_command = 'dbt run --profiles-dir=/path/to/your/profiles --models your_model_name'

subprocess.run(dbt_command, shell=True, check=True)

# Step 3: Load transformed data into BigQuery
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'path-to-your-service-account-key.json'
client = bigquery.Client(project=project_id)

# Load transformed data into BigQuery table
table_ref = client.dataset(dataset_id).table(table_id)
job_config = bigquery.LoadJobConfig(
    source_format=bigquery.SourceFormat.CSV,
    autodetect=True,
)

with open('/tmp/data.csv', 'rb') as source_file:
    job = client.load_table_from_file(source_file, table_ref, job_config=job_config)

job.result()  # Wait for the job to complete

print(f"Loaded {job.output_rows} rows into {project_id}.{dataset_id}.{table_id}")
